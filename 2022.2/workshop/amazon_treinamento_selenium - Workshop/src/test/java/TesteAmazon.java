import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TesteAmazon {
    private WebDriver driver;

    @Before
public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/Windows/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br");
    }
     @After
     public void sair() {
     driver.quit();
     }

     @Test
     public void clicarMaisVendidos() throws InterruptedException{
     driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[2]")).click();
         Assert.assertEquals( "Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
     }
}